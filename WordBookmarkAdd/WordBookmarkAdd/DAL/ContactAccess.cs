﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.DTO;
using CustomerContact = WordBookmarkAdd.DAL.EF.CustomerContact;
using VendorContact = WordBookmarkAdd.DAL.EF.VendorContact;

namespace WordBookmarkAdd.DAL
{
    public class ContactAccess
    {
        private static ContactAccess _Instance;
        private static object lockObj = new object();

        private ContactAccess()
        {
        }

        public static ContactAccess Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock (lockObj)
                    {
                        if (_Instance == null)
                        {
                            _Instance = new ContactAccess();
                        }
                    }
                }
                return _Instance;
            }
        }

        public DTO.CustomerContact AddCustomerInformation(string email, string name, string phone, DocModulRef company, string title, long module)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    CustomerContact cust = new CustomerContact();
                    DocModulContacts dmc = null;
                    cust.Mail = email;
                    cust.Name = name;
                    cust.Phone = phone;
                    cust.Customer = company.No;
                    cust.Title = title;
                    context.CustomerContact.Add(cust);
                    context.SaveChanges();

                    dmc = context.DocModulContacts.First(d => d.Modul == 1 && d.Contact == cust.ID);
                    transaction.Commit();

                    return contactFrom(cust,dmc);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    Logging.NLogger.Instance.Logger.Error(e);
                    throw;
                }
            }
        }

        public DTO.VendorContact AddVendorInformation(string email, string name, string phone, DocModulRef modref, string title, string mobile, long module)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            using (var transaction = context.Database.BeginTransaction())
            {

                try
                {
                    VendorContact vend = new VendorContact();
                    DocModulContacts dmc = new DocModulContacts(); ;
                    vend.Mail = email;
                    vend.Name = name;
                    vend.Phone = phone;
                    vend.Vendor = modref.No;
                    vend.Title = title;
                    vend.Mobile = mobile;
                    vend.Active = true;
                    vend.Created = DateTime.Now;
                    context.VendorContact.Add(vend);
                    //assume this is needed to get ID
                    context.SaveChanges();

                    dmc.Modul = module;
                    dmc.Email = email;
                    dmc.Name = name;
                    dmc.No = modref.No;
                    dmc.Contact = vend.ID;

                    context.DocModulContacts.Add(dmc);
                    context.SaveChanges();
                    transaction.Commit();
                    return contactFrom(vend, dmc);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    Logging.NLogger.Instance.Logger.Error(e);
                    throw;
                }
            }
        }

        private DTO.VendorContact contactFrom(EF.VendorContact vendorContact, DocModulContacts contact)
        {
            return new DTO.VendorContact()
            {
                ID = contact.ID,
                Vendor_ID = contact.No.Value,
                VendorContact_ID = contact.Contact.Value,
                Email = contact.Email,
                Name = contact.Name,
                Phone = vendorContact.Phone,
                Modul = contact.Modul.Value,
                Title = vendorContact.Title,
            };
        }
        private DTO.CustomerContact contactFrom(EF.CustomerContact customerContact, DocModulContacts contact)
        {
            return new DTO.CustomerContact()
            {
                ID = contact.ID,
                Company = contact.No.Value,
                CustomerContact_ID = contact.Contact.Value,
                Email = contact.Email,
                Name = contact.Name,
                Phone = customerContact.Phone,
                Modul = contact.Modul.Value,
                Title = customerContact.Title
            };
        }

        

        public ModuleContact AddDocModuleRefInformation(string email, string name, long? CompanyID, long module)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var dmc = new DocModulContacts();
                dmc.Modul = module;
                dmc.Email = email;
                dmc.Name = name;
                dmc.No = CompanyID;
                dmc.Contact = null;
                context.DocModulContacts.Add(dmc);
                context.SaveChanges();
                return new ModuleContact()
                {
                    Contact = dmc.Contact,
                    Name = dmc.Name,
                    Email = dmc.Email,
                    ID = dmc.ID,
                    Modul = dmc.Modul.Value,
                    Company = dmc.No
                };
            }
        }
    }
}
