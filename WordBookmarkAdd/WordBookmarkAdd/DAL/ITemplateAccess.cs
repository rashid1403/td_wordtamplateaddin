﻿using System.Collections.Generic;
using WordBookmarkAdd.DAL.EF;

namespace WordBookmarkAdd.DAL
{
    public interface ITemplateAccess
    {
        /// <summary>
        /// Method used to fetch all the templates from the database
        /// </summary>
        /// <returns>A list containing the templates</returns>
        List<DocTemplates> GetAllTemplates();
    }
}
