﻿using System;
using System.Data.SqlClient;
using WordBookmarkAdd.BLL;

namespace WordBookmarkAdd.DAL
{
    public class SettingsParser : ISettingsParser
    {
        private static readonly Lazy<SettingsParser> Lazy = new Lazy<SettingsParser>(() => new SettingsParser());

        public static ISettingsParser Instance => Lazy.Value;

        private SettingsParser()
        {
        }

        public bool TestDatabaseConnection(string server, string db, string user, string pw, bool integratedSecurity)
        {
            var sqlConString = new SqlConnectionStringBuilder
            {
                InitialCatalog = db,
                DataSource = server,
                IntegratedSecurity = integratedSecurity,
                UserID = user,
                Password = pw
            };

            return SettingsHandler.Instance.TestDatabaseConnection(sqlConString.ConnectionString);
        }

        public bool SaveDatabaseConnection(string server, string db, string user, string pw, bool integratedSecurity)
        {
            var sqlConString = new SqlConnectionStringBuilder
            {
                InitialCatalog = db,
                DataSource = server,
                IntegratedSecurity = integratedSecurity,
                UserID = user,
                Password = pw
            };

            return SettingsHandler.Instance.SaveConfig(sqlConString.ConnectionString);
        }
    }
}
