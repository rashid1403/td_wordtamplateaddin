﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.Logging;

namespace WordBookmarkAdd.DAL
{
    public class DocumentAccess : IDocumentAccess
    {
        private static DocumentAccess _Instance;
        private static object lockForSingleton = new object();


        private static readonly Lazy<DocumentAccess> Lazy = new Lazy<DocumentAccess>(() => new DocumentAccess());

        //public static IDocumentAccess Instance = Lazy.Value;


        public static DocumentAccess Instance
        {
            get
            {
                if (_Instance != null)
                {
                    return _Instance;
                }
                lock (lockForSingleton)
                {
                    return _Instance ?? (_Instance = new DocumentAccess());
                }
            }
        }

        private DocumentAccess()
        {
        }

        /// <summary>
        /// only searches Customers NOT Vendors !!! 1
        /// </summary>
        /// <param name="nameFragment"></param>
        /// <param name="listSize"></param>
        /// <returns></returns>
        public List<Tuple<string, long>> SearchCompany(string nameFragment, int listSize = 5)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var list = context.Customer
                    .Where(c => c.Name.StartsWith(nameFragment))
                    .Select(c => new { Name = c.Name, ID = c.ID })
                    .OrderBy(c => c.Name)
                    .Take(listSize);
                return list.AsEnumerable().Select(c => Tuple.Create(c.Name, c.ID)).ToList();
            }
        }

        public List<DocModulRef> searchCompany(string text, long modul, int listSize = 5)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
                try
                {
                    return context.DocModulRef.Where(r => r.Modul == modul).OrderBy(r => r.Name).Where(r => r.Name.StartsWith(text)).Take(listSize).ToList();
                }
                catch (Exception exe)
                {
                    NLogger.Instance.Logger.Error(exe);
                    throw;
                }
        }

        public List<DocModulRef> SearchModuleRefs(string text)
        {
            try
            {
                using (var context = DBContextHandler.GetNewDBInstance())
                {
                    return context.DocModulRef.Where(dmr => dmr.Name.Contains(text)).OrderBy(dmr => dmr.Name).ToList();
                }
            }
            catch (Exception exe)
            {
                NLogger.Instance.Logger.Error(exe);
                throw;
            }
            
        }

        public void SaveDocument(Documents document)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Documents.Add(document);
                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        NLogger.Instance.Logger.Error(ex);
                        throw;
                    }
                }
            }
        }

       

        public List<DocStructure> GetStructures(long module)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                try
                {
                    return context.DocStructure.Where(ds => ds.DocModule == module).ToList();
                }
                catch (Exception e)
                {
                    NLogger.Instance.Logger.Error(e);
                    throw;
                }
            }
        }

        public SystemParameter DocPath()
        {
            using (var docPath = DBContextHandler.GetNewDBInstance())
            {
                var p = docPath.SystemParameter.Where(path => path.ID == 10);
                return p.FirstOrDefault();
            }
        }

        public void SaveModuleRef(DocModulRef moduleRef)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                try
                {
                    context.DocModulRef.Add(moduleRef);
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    NLogger.Instance.Logger.Error(e);
                    throw;
                }
            }
        }
    }
}
