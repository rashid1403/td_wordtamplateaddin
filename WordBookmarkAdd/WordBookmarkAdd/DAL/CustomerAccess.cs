﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NLog;
using NLog.LayoutRenderers.Wrappers;
using WordBookmarkAdd.BE;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.Logging;

namespace WordBookmarkAdd.DAL
{
    public class CustomerAccess : ICustomerAccess
    {
        private static readonly Lazy<CustomerAccess> Lazy = new Lazy<CustomerAccess>(() => new CustomerAccess());

        public static ICustomerAccess Instance = Lazy.Value;

        private CustomerAccess()
        {
        }

        public BE_Customer GetCustomer(long? id)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var query = from customer in context.Customer.Where(cust => cust.ID == id)
                    from zip in context.CountryZipCode.Where(z => z.ID == customer.Zip).DefaultIfEmpty()
                    from country in context.Country.Where(c => c.ID == customer.Country.Value).DefaultIfEmpty()
                    
                    select new BE_Customer
                    {
                        Id = customer.ID,
                        CompanyName = customer.Name,
                        CompanyAddress = customer.Adress3 ?? customer.Adress1 ?? customer.Adress2,
                        CompanyCity = zip.City,
                        CompanyPhone = customer.Phone,
                        CompanyZip = zip.Zip,
                        CompanyCountry = country.Country1
                    };

                return query.FirstOrDefault();
            }
        }

        public List<CustomerContact> SearchForCustomersContact(string PersonName, long Company)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
           try
                {
                    //var query = from cust in context.Customer
                    //         join custCon in context.CustomerContact 
                    //         on cust.ID equals custCon.Customer select new
                    //         {
                    //             custConName = custCon.Name.Contains(text)
                    //         };

                    var result = context.CustomerContact
                        .Where(x => x.Customer1.ID == Company && x.Name.Contains(PersonName))
                        .ToList();

                   // context.CustomerContact.Where(cc => cc.Name.Contains(text)).OrderBy(cc => cc.Name).Take(30).ToList();

                    //return context.CustomerContact.Where (cc => cc.Customer = ) (cc => cc.Name.Contains(text)).OrderBy (cc => cc.Name).Take(30).ToList();
                   // return query.Where(cc => cc.custConName.Contains(text)).ToList();
                    return result;
                }
                catch (Exception e)
                {
                    Logging.NLogger.Instance.Logger.Error(e);
                    throw;
                }
            }
        }

        public List<Tuple<string, long>> FindAllStatuses()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var list = context.CustomerStatus.ToList();
                return list.Select(status => Tuple.Create(status.Status, status.ID)).ToList();
            }
        }

        public List<Tuple<string, long>> FindAllCustomerGroups()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var list = context.CustomerGroup.ToList();
                return list.Select(status => Tuple.Create(status.CustomerGroup1, status.ID)).ToList();
            }
        }


        public List<Tuple<string, long>> FindAllCustomerSubGroups(long groupID)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var list = context.CustomerGroupSub.Where(cg => cg.CustomerGroup == groupID).ToList();
                return list.Select(sg => Tuple.Create(sg.SubGroup, sg.ID)).ToList();
            }
        }

        public void AddNewCustomer(Customer cust)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                try
                {
                    cust.CreatedDate = DateTime.Now;
                    context.Customer.Add(cust);
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    NLogger.Instance.Logger.Error(e);
                    throw;
                }
            }
        }

        public List<DocModule> GetAllModul()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var r = context.DocModule.Select(reff => reff);
                return r.ToList();
            }
        }
        //public List<CustomerContact> SearchForCustomersContactID(string text)
        //{
        //    using (var context = DBContextHandler.GetNewDBInstance())
        //    {
        //        try
        //        {
        //            return context.CustomerContact.Where(cc => cc.Name.Contains(text)).OrderBy(cc => cc.Name).Take(30).ToList();
        //        }
        //        catch (Exception e)
        //        {
        //            Logging.NLogger.Instance.Logger.Error(e);
        //            throw;
        //        }
        //    }
        //}

    }
}
