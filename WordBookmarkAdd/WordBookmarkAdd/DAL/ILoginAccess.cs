﻿using System.Collections;
using System.Collections.Generic;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.DTO;

namespace WordBookmarkAdd.DAL
{
    public interface ILoginAccess
    {

        /// <summary>
        /// Returns a dictionary of logins
        /// </summary>
        /// <returns>Returns a dictionary of logins where the key is id and value is an user object</returns>
        List<login> GetLogins();

        /// <summary>
        /// Retrieves the first matching user from the database
        /// Tries to match the login name in the table with the current logged in machine user
        /// </summary>
        /// <returns>Returns a login object if a user could be found in the database, returns null otherwise</returns>
        login GetCurrentUser();

        List<login> SearchLogin(string text);
    }
}
