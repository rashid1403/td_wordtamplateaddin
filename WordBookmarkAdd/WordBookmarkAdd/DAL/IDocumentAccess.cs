﻿using System.Collections.Generic;
using WordBookmarkAdd.DAL.EF;

namespace WordBookmarkAdd.DAL
{
    public interface IDocumentAccess
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        List<DocModulRef> SearchModuleRefs(string text);
        

        SystemParameter DocPath();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        void SaveDocument(Documents document);

        List<DAL.EF.DocStructure> GetStructures(long moduleID);

    }
}
