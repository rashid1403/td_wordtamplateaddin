﻿using WordBookmarkAdd.DAL.EF;

namespace WordBookmarkAdd.DAL
{
    public interface IQuotationAccess
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quotation"></param>
        void SaveQuotation(QuotationQuick quotation);
    }
}
