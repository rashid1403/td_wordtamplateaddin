﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.Logging;

namespace WordBookmarkAdd.DAL
{
    public class VendorAccess
    {
        private static VendorAccess _instance;
        private static readonly object LockForSingleton = new object();

        private VendorAccess()
        {
        }

        public static VendorAccess Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (LockForSingleton)
                    {
                        if (_instance == null)
                        {
                            _instance = new VendorAccess();
                        }
                    }
                }
                return _instance;
            }
        }

        public void AddVendor(Vendor vend)
        {
            try
            {
                using (var context = DBContextHandler.GetNewDBInstance())
                {
                    context.Vendor.Add(vend);
                    context.SaveChanges();
                }
            }
            catch (Exception exe)
            {
                NLogger.Instance.Logger.Error(exe);
                throw;
            }
        }
    }
}
