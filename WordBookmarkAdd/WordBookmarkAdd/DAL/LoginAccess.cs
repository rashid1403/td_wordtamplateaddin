﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.DTO;

namespace WordBookmarkAdd.DAL
{
    public class LoginAccess : ILoginAccess
    {
        private static readonly Lazy<LoginAccess> Lazy = new Lazy<LoginAccess>(() => new LoginAccess());

        public static ILoginAccess Instance = Lazy.Value;

        private LoginAccess()
        {
        }

        public login GetCurrentUser()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                return context.login.FirstOrDefault(l => l.login1 == Environment.UserName);
            }
        }

        public List<login> SearchLogin(string text)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                return context.login.Where(l => l.UserName.Contains(text)).ToList();
            }
        }

        public List<login> GetLogins()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                return context.login.ToList();
            }
        }
    }
}
