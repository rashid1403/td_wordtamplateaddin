﻿using System;
using System.Collections;
using System.Collections.Generic;
using WordBookmarkAdd.BE;
using WordBookmarkAdd.DAL.EF;

namespace WordBookmarkAdd.DAL
{
    public interface ICustomerAccess
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BE_Customer GetCustomer(long? id);
        
        List<CustomerContact> SearchForCustomersContact(string name, long CompanyID);

        List<Tuple<string, long>> FindAllStatuses();

        List<Tuple<string, long>> FindAllCustomerGroups();

        List<Tuple<string, long>> FindAllCustomerSubGroups(long groupID);
        void AddNewCustomer(Customer cust);
        List<DocModule> GetAllModul();
    }
}
