﻿using System;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.Logging;

namespace WordBookmarkAdd.DAL
{
    public class QuotationAccess : IQuotationAccess
    {
        private static readonly Lazy<QuotationAccess> Lazy = new Lazy<QuotationAccess>(() => new QuotationAccess());

        public static IQuotationAccess Instance = Lazy.Value;

        private QuotationAccess()
        {
        }

        public void SaveQuotation(QuotationQuick quotation)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.QuotationQuick.Add(quotation);
                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        NLogger.Instance.Logger.Error(ex);
                        throw;
                    }
                }
            }
        }
    }
}
