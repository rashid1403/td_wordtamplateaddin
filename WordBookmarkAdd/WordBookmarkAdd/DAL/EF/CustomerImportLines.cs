//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WordBookmarkAdd.DAL.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerImportLines
    {
        public long ID { get; set; }
        public string ImportRef { get; set; }
        public string Name { get; set; }
        public string Adress1 { get; set; }
        public string Adress2 { get; set; }
        public string Adress3 { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public Nullable<int> Ranking { get; set; }
        public string Currency { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string www { get; set; }
        public string VatRegNo { get; set; }
        public string Remarks { get; set; }
        public string Linestatus { get; set; }
        public Nullable<long> Errortext { get; set; }
    
        public virtual CustomerImport CustomerImport { get; set; }
        public virtual Errortexts Errortexts { get; set; }
    }
}
