//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WordBookmarkAdd.DAL.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesTeamBudgetTrans
    {
        public long Lineid { get; set; }
        public Nullable<long> RefID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<decimal> Amount { get; set; }
    }
}
