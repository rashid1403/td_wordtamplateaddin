//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WordBookmarkAdd.DAL.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesTeamBudget
    {
        public long ID { get; set; }
        public Nullable<long> SalesTeam { get; set; }
        public Nullable<long> Year { get; set; }
        public Nullable<long> Period { get; set; }
        public Nullable<long> Responsible { get; set; }
        public Nullable<long> Country { get; set; }
        public Nullable<long> Currency { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> Jan { get; set; }
        public Nullable<decimal> Feb { get; set; }
        public Nullable<decimal> Mar { get; set; }
        public Nullable<decimal> Apr { get; set; }
        public Nullable<decimal> May { get; set; }
        public Nullable<decimal> Jun { get; set; }
        public Nullable<decimal> Jul { get; set; }
        public Nullable<decimal> Aug { get; set; }
        public Nullable<decimal> Sep { get; set; }
        public Nullable<decimal> Oct { get; set; }
        public Nullable<decimal> Nov { get; set; }
        public Nullable<decimal> Dec { get; set; }
    
        public virtual BudgetYear BudgetYear { get; set; }
        public virtual BudgetYearPeriods BudgetYearPeriods { get; set; }
        public virtual Country Country1 { get; set; }
        public virtual Currency Currency1 { get; set; }
        public virtual login login { get; set; }
        public virtual SalesTeam SalesTeam1 { get; set; }
    }
}
