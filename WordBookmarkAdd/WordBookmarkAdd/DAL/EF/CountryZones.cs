//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WordBookmarkAdd.DAL.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class CountryZones
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CountryZones()
        {
            this.CountryZipCode = new HashSet<CountryZipCode>();
            this.ItemCostprices = new HashSet<ItemCostprices>();
            this.ItemCostprices1 = new HashSet<ItemCostprices>();
        }
    
        public long ID { get; set; }
        public Nullable<long> Country { get; set; }
        public string Zone { get; set; }
    
        public virtual Country Country1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CountryZipCode> CountryZipCode { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ItemCostprices> ItemCostprices { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ItemCostprices> ItemCostprices1 { get; set; }
    }
}
