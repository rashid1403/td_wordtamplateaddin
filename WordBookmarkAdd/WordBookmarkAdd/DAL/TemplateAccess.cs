﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordBookmarkAdd.DAL.EF;

namespace WordBookmarkAdd.DAL
{
    public class TemplateAccess : ITemplateAccess
    {
        private static readonly Lazy<TemplateAccess> Lazy = new Lazy<TemplateAccess>(() => new TemplateAccess());

        public static ITemplateAccess Instance = Lazy.Value;

        private TemplateAccess()
        {
        }

        public List<DocTemplates> GetAllTemplates()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                return context.DocTemplates.ToList();
            }
        }
    }
}
