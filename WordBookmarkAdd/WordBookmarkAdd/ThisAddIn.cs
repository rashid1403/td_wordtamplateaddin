﻿using System.Windows;
using WordBookmarkAdd.BLL;
using WordBookmarkAdd.GUI.StandAlone;

namespace WordBookmarkAdd
{
    public partial class ThisAddIn
    {
        private readonly ISettingsHandler _settingsHandler = SettingsHandler.Instance;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(_settingsHandler.ReadConfig(_settingsHandler.GetCurrentWindowsUser())))
            {
                const string message = "The database connection has not yet been been set.\nDo you want to setup the Connection?";
                const string title = "Thorvald Data FlexCRM - Database Setup";
                var mb = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

                if (mb == MessageBoxResult.Yes)
                    DatabaseConnection.CreateWindow();
            }
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
