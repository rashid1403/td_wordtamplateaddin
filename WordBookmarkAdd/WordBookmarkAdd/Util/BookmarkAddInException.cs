﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordBookmarkAdd.Util
{
    public class BookmarkAddInException : Exception
    {
        public BookmarkAddInException(string msg) : base(msg)
        {
            
        }
    }
}
