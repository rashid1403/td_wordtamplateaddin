﻿using System;

namespace WordBookmarkAdd.Util
{
    public class SharedVariables
    {
        private static readonly Lazy<SharedVariables> Lazy = new Lazy<SharedVariables>(() => new SharedVariables());

        public static SharedVariables Instance => Lazy.Value;

        private SharedVariables()
        {
        }

        // Registry stuff
        public string RootKey = @"SOFTWARE\ThorvaldData\WordBookmarkAddIn\";
        public string SubKey = "%NAME%";
        public string KeyName = "ConnectionString";

        // Database stuff
        public string WinSecurity = "integratedSecurity";
        public string Server = "server";
        public string Database = "db";
        public string User = "user";
        public string Password = "pw";
    }
}
