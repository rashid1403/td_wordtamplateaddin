﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Core;

namespace WordBookmarkAdd.Util
{
    public class CustomDocProps
    {
        private static DocumentProperties props => (DocumentProperties)Globals.ThisAddIn.Application.ActiveDocument.CustomDocumentProperties;

        public static string CompanyName
        {
            get
            {
                return props[nameof(CompanyName)].Value.ToString();
            }
            set
            {
                props?.Add(nameof(CompanyName), false, MsoDocProperties.msoPropertyTypeString, value);
            }
        }

        public static string CompanyAddress
        {
            get
            {
                return props[nameof(CompanyAddress)].Value.ToString();
            }
            set
            {
                props?.Add(nameof(CompanyAddress), false, MsoDocProperties.msoPropertyTypeString, value);
            }
        }

        public static string CompanyPhone
        {
            get
            {
                return props[nameof(CompanyPhone)].Value.ToString();
            }
            set
            {
                props?.Add(nameof(CompanyPhone), false, MsoDocProperties.msoPropertyTypeString, value);
            }
        }

        public static string CompanyZip
        {
            get
            {
                return props[nameof(CompanyZip)].Value.ToString();
            }
            set
            {
                props?.Add(nameof(CompanyZip), false, MsoDocProperties.msoPropertyTypeString, value);
            }
        }

        public static string CompanyCity
        {
            get
            {
                return props[nameof(CompanyCity)].Value.ToString();
            }
            set
            {
                props?.Add(nameof(CompanyCity), false, MsoDocProperties.msoPropertyTypeString, value);
            }
        }

        public static string CompanyCountry
        {
            get
            {
                return props[nameof(CompanyCountry)].Value.ToString();
            }
            set
            {
                props?.Add(nameof(CompanyCountry), false, MsoDocProperties.msoPropertyTypeString, value);
            }
        }

        public static string ContactName
        {
            get
            {
                return props[nameof(ContactName)].Value.ToString();
            }
            set
            {
                props?.Add(nameof(ContactName), false, MsoDocProperties.msoPropertyTypeString, value);
            }
        }

        public static string ContactPhone
        {
            get
            {
                return props[nameof(ContactPhone)].Value.ToString();
            }
            set
            {
                props?.Add(nameof(ContactPhone), false, MsoDocProperties.msoPropertyTypeString, value);
            }
        }

        public static string ContactMobile
        {
            get
            {
                return props[nameof(ContactMobile)].Value.ToString();
            }
            set
            {
                props?.Add(nameof(ContactMobile), false, MsoDocProperties.msoPropertyTypeString, value);
            }
        }

        public static string ContactMail
        {
            get
            {
                return props[nameof(ContactMail)].Value.ToString();
            }
            set
            {
                props?.Add(nameof(ContactMail), false, MsoDocProperties.msoPropertyTypeString, value);
            }
        }

        public static long? DocumentModule
        {
            get
            {
                return (long?)props[nameof(DocumentModule)].Value;
            }
            set
            {
                props?.Add(nameof(DocumentModule), false, MsoDocProperties.msoPropertyTypeNumber, value);
            }
        }
        public static long? CompanyNo
        {
            get
            {
                return (long?)props[nameof(CompanyNo)].Value;
            }
            set
            {
                props?.Add(nameof(CompanyNo), false, MsoDocProperties.msoPropertyTypeNumber, value);
            }
        }

        public static long? CompanyModule
        {
            get
            {
                return (long?)props[nameof(CompanyModule)].Value;
            }
            set
            {
                props?.Add(nameof(CompanyModule), false, MsoDocProperties.msoPropertyTypeNumber, value);
            }
        }

        public static long CompanyRefId
        {
            get
            {
                return (long)props[nameof(CompanyRefId)].Value;
            }
            set
            {
                props?.Add(nameof(CompanyRefId), false, MsoDocProperties.msoPropertyTypeNumber, value);
            }
        }
    }
}
