﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using WordBookmarkAdd.DAL;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.Util;

namespace WordBookmarkAdd.BLL
{
    public class DocLocation
    {
        private Logger log = LogManager.GetCurrentClassLogger();
        private string tailPath;
        //string templatesType, string company, out string tailPath
        private string docLocation;

        public string FileName { get; private set; }


        public void DocInformationReader(string documentModuel, string companyName, string documentType)
        {
            string headPath = DocumentAccess.Instance.DocPath().Value;
            if (string.IsNullOrWhiteSpace(headPath))
            {
                var ex = new BookmarkAddInException("No Document path set in Database");
                //log.Error(ex);
                throw ex;
            }

                
            tailPath = "\\" + documentModuel + "\\" + CleanString(companyName) + "\\" + documentType;

            docLocation = headPath + tailPath;
            Directory.CreateDirectory(docLocation);
            FileName = CleanString(companyName) + "_" + CleanString(DateTime.Now.ToString()) + ".docx";
        }
        public string DocumentLocation()
        {
            return docLocation;
        }

        public string GetTailPath()
        {
            return tailPath;
        }
        

        /// <summary>
        /// Clean text if contains this character.
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        private string CleanString(string module)
        {
            string illegal = "\\/.,:";
            StringBuilder builder = new StringBuilder(module);
            foreach (char c in illegal)
            {
                builder.Replace(c.ToString(), "");
            }
            return builder.ToString();
        }
    }
}
