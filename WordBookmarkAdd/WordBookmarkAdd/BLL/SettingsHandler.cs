﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.Win32;
using WordBookmarkAdd.Logging;
using WordBookmarkAdd.Util;

namespace WordBookmarkAdd.BLL
{
    public class SettingsHandler : ISettingsHandler
    {
        private static readonly Lazy<SettingsHandler> Lazy = new Lazy<SettingsHandler>(() => new SettingsHandler());

        public static ISettingsHandler Instance => Lazy.Value;

        private SettingsHandler()
        {
        }

        public string GetCurrentWindowsUser()
        {
            return Environment.UserName;
        }

        /// <summary>
        /// Replaces the %NAME% in the subkey with the parameter
        /// </summary>
        /// <param name="username">%NAME% is replaced with the username</param>
        /// <returns></returns>
        private string ReplaceKeyWithUser(string username)
        {
            string rootKey = SharedVariables.Instance.RootKey;
            string subKey = SharedVariables.Instance.SubKey;
            string key = rootKey + subKey;
            return key.Replace(subKey, username);
        }

        public string ReadConfig(string username)
        {
            try
            {
                using (var key = Registry.CurrentUser.OpenSubKey(ReplaceKeyWithUser(username)))
                {
                    return key?.GetValue(SharedVariables.Instance.KeyName)?.ToString();
                }
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                throw;
            }
        }

        public bool SaveConfig(string constring, string username)
        {
            try
            {
                using (var key = Registry.CurrentUser.CreateSubKey(ReplaceKeyWithUser(username)))
                {
                    key?.SetValue(SharedVariables.Instance.KeyName, constring);
                    return true;
                }
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                return false;
            }
        }

        public bool SaveConfig(string constring)
        {
            return SaveConfig(constring, GetCurrentWindowsUser());
        }

        public bool DeleteConfig(string username)
        {
            string rootKey = SharedVariables.Instance.RootKey;

            try
            {
                using (var key = Registry.CurrentUser.OpenSubKey(rootKey, true))
                {
                    if (key?.OpenSubKey(username) != null)
                    {
                        key.DeleteSubKeyTree(username);
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                return false;
            }
        }

        public bool DatabaseSetup(string username)
        {
            return TestDatabaseConnection(ReadConfig(username));
        }

        public bool DatabaseSetup()
        {
            return DatabaseSetup(GetCurrentWindowsUser());
        }

        public bool TestDatabaseConnection(string connString)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connString))
                {
                    sqlConnection.Open();
                    return true;
                }
            }
            catch (SqlException ex)
            {
                NLogger.Instance.Logger.Error(ex);
                throw;
            }
            catch (Exception ex) when (ex is ArgumentException || ex is InvalidOperationException)
            {
                return false;
            }
        }

        public Dictionary<string, string> GetConnectionInfo()
        {
            var connProps = new Dictionary<string, string>();

            try
            {
                string connectionString = ReadConfig(GetCurrentWindowsUser());
                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(connectionString);

                string winSecurity = SharedVariables.Instance.WinSecurity;
                string server = SharedVariables.Instance.Server;
                string database = SharedVariables.Instance.Database;
                string user = SharedVariables.Instance.User;
                string password = SharedVariables.Instance.Password;

                connProps.Add(winSecurity, sqlConnectionStringBuilder.IntegratedSecurity.ToString());
                connProps.Add(server, sqlConnectionStringBuilder.DataSource);
                connProps.Add(database, sqlConnectionStringBuilder.InitialCatalog);
                connProps.Add(user, sqlConnectionStringBuilder.UserID);
                connProps.Add(password, sqlConnectionStringBuilder.Password);
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                throw;
            }
            return connProps;
        }
    }
}
