﻿using System;
using Microsoft.Office.Interop.Word;

namespace WordBookmarkAdd.BLL
{
    public class WordLogic
    {
        private static readonly Lazy<WordLogic> Lazy = new Lazy<WordLogic>(() => new WordLogic());

        public static WordLogic Instance => Lazy.Value;

        private WordLogic()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="bookmarkName"></param>
        /// <returns></returns>
        public string GetBookmarkText(Document doc, string bookmarkName)
        {
            if (doc.Bookmarks.Exists(bookmarkName))
            {
                object name = bookmarkName;
                Bookmark bm = doc.Bookmarks.get_Item(ref name);

                return bm.Range.Text;
            }
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="bm"></param>
        /// <param name="text"></param>
        public void UpdateBookmark(Document doc, Bookmark bm, string text)
        {
            //StringBuilder sb = new StringBuilder();
            int bmStart = bm.Start;
            string bmName = bm.Name;
            //string tmpText = doc.Range(bmStart, bm.End).Text;

            if (text != null)
            {
                //if (tmpText.Length > text.Length)
                //{
                //    int difference = tmpText.Length - text.Length;
                //    sb.Append(' ', difference);
                //    text += sb.ToString();
                //}

                doc.Range(bmStart, bm.End).Text = text;
                doc.Bookmarks.Add(bmName, doc.Range(bmStart, bmStart + text.Length));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="bookmarkName"></param>
        /// <returns></returns>
        public Bookmark GetBookmark(Document doc, string bookmarkName)
        {
            if (doc.Bookmarks.Exists(bookmarkName))
            {
                object name = bookmarkName;
                return doc.Bookmarks.get_Item(ref name);
            }
            return null;
        }
    }
}
