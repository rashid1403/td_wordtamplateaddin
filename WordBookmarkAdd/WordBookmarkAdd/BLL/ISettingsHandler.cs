﻿using System.Collections.Generic;

namespace WordBookmarkAdd.BLL
{
    public interface ISettingsHandler
    {
        /// <summary>
        /// Returns the user name of the current windows user
        /// </summary>
        /// <returns>Username in the form of a string</returns>
        string GetCurrentWindowsUser();

        /// <summary>
        /// Reads the database configuration from the registry
        /// </summary>
        /// <param name="username">The username in the registry</param>
        /// <returns>Returns the registry value for the current user</returns>
        string ReadConfig(string username);

        /// <summary>
        /// Saves the configuration to the registration database
        /// </summary>
        /// <param name="constring">Databasse connection string</param>
        /// <param name="username">The user in the registry</param>
        /// <returns>Returns true or false depending on whether or not the database configuration could be saved in the registry</returns>
        bool SaveConfig(string constring, string username);

        /// <summary>
        /// Overload method, which saves with the current user
        /// </summary>
        /// <param name="constring">Database connection string</param>
        /// <returns>Returns true or false depending on whether or not the database configuration could be saved in the registry</returns>
        bool SaveConfig(string constring);

        /// <summary>
        /// Deletes the database configuration in the registry database,
        /// For the user with the Username as subkey
        /// </summary>
        /// <param name="username">The user in the registry</param>
        /// <returns>Returns true if the subkey in the registry could be deleted, false if it couldn't be deleted or if it doesn't exist</returns>
        bool DeleteConfig(string username);

        /// <summary>
        /// General method which reads the configuration from the username
        /// Converts the entity framework connection string to sql
        /// And tests the connection, and returns true or false depending on the result
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>Returns true if the database connection was tested successfully, false if it failed</returns>
        bool DatabaseSetup(string username);

        /// <summary>
        /// Overload method, which tests the database setup with the current user
        /// </summary>
        /// <returns>Returns true if the database connection was tested successfully, false if it failed</returns>
        bool DatabaseSetup();

        /// <summary>
        /// Tests the database connecton with the supplied connection string,
        /// And returns true or false depending on the result.
        /// </summary>
        /// <param name="connString">The SQL connection string</param>
        /// <returns>/// <returns>Returns true if the database connection was tested successfully, false if it failed</returns></returns>
        bool TestDatabaseConnection(string connString);

        /// <summary>
        /// Method used to get the connection info from the current user,
        /// And return a dictionary with the information
        /// Server, database, username, password
        /// </summary>
        /// <returns>Returns a dictionary containing predefined keys, and values from the sql connection string read from the registry</returns>
        Dictionary<string, string> GetConnectionInfo();
    }
}
