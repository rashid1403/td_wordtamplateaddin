﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordBookmarkAdd.BE
{
    public class BE_Customer
    {
        public long Id { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyZip { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyCountry { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactMobile { get; set; }
        public string ContactMail { get; set; }
    }
}
