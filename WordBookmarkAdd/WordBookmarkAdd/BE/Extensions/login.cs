﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordBookmarkAdd.DAL.EF
{
    partial class login
    {
        protected bool Equals(login other)
        {
            return ID == other.ID;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((login) obj);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        private sealed class IdEqualityComparer : IEqualityComparer<login>
        {
            public bool Equals(login x, login y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.ID == y.ID;
            }

            public int GetHashCode(login obj)
            {
                return obj.ID.GetHashCode();
            }
        }

        private static readonly IEqualityComparer<login> IdComparerInstance = new IdEqualityComparer();

        public static IEqualityComparer<login> IdComparer
        {
            get { return IdComparerInstance; }
        }
    }
}
