﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordBookmarkAdd.DAL.EF
{
    partial class CustomerContact
    {
        private sealed class IdEqualityComparer : IEqualityComparer<CustomerContact>
        {
            public bool Equals(CustomerContact x, CustomerContact y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.ID == y.ID;
            }

            public int GetHashCode(CustomerContact obj)
            {
                return obj.ID.GetHashCode();
            }
        }

        private static readonly IEqualityComparer<CustomerContact> IdComparerInstance = new IdEqualityComparer();

        public static IEqualityComparer<CustomerContact> IdComparer
        {
            get { return IdComparerInstance; }
        }

        protected bool Equals(CustomerContact other)
        {
            return ID == other.ID;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((CustomerContact) obj);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }
    }
}
