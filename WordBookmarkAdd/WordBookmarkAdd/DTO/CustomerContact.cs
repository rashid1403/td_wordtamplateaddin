﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordBookmarkAdd.DTO
{
    public class CustomerContact : ModuleContact, IContact
    {
        //intarface members
        public string Phone { get; set; }
        public string Title { get; set; }

        //not
        public long CustomerContact_ID { get; set; }
    }
}
