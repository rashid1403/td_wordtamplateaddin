﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordBookmarkAdd.DTO
{
    public class ModuleContact
    {
        public long ID { get; set; }
        public long Modul { get; set; }
        public long? Company { get; set; }
        public long? Contact { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
