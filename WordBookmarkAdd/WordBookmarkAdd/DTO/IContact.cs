﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordBookmarkAdd.DTO
{
    public interface IContact
    {
        string Phone { get; set; }
        string Title { get; set; }
    }
}
