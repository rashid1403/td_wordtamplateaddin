﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using WordBookmarkAdd.BLL;
using WordBookmarkAdd.DAL;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.Util;
using Window = System.Windows.Window;

namespace WordBookmarkAdd.GUI.StandAlone.TemplateChildren
{
    /// <summary>
    /// Interaction logic for CompanySelection.xaml
    /// </summary>
    public partial class CompanySelection : UserControl
    {
        private RelayCommand _searchCommand;
        private RelayCommand _okCommand;
        private readonly ITemplateAccess _templateAccess = TemplateAccess.Instance;
        private readonly ICustomerAccess _customerAccess = CustomerAccess.Instance;
        private readonly ILoginAccess _loginAccess = LoginAccess.Instance;
        private readonly WordLogic _wordLogic = WordLogic.Instance;
        private string _windowTitle = "Select Company";

        public TemplateParent TParent { get; set; }

        public CompanySelection()
        {
            InitializeComponent();
            lstCompanies.SelectedValuePath = nameof(DocModulRef.ID);
            lstCompanies.DisplayMemberPath = nameof(DocModulRef.Name);

            cmbTemplate.SelectedValuePath = nameof(DocTemplates.FileLink);
            cmbTemplate.DisplayMemberPath = nameof(DocTemplates.Name);
            cmbTemplate.ItemsSource = _templateAccess.GetAllTemplates();

            btnSearch.Command = SearchCommand;
            btnNext.Command = NextCommand;
        }

        private ICommand SearchCommand
        {
            get
            {
                return _searchCommand ??
                       (_searchCommand =
                           new RelayCommand(param => Search()));
            }
        }

        private ICommand NextCommand
        {
            get
            {
                return _okCommand ?? (_okCommand =
                    new RelayCommand(param => NextExecuted(), param => NextCanExecute));
            }
        }

        private void NextExecuted()
        {
            TParent.FirstWindowNext();
        }

        private bool NextCanExecute => cmbTemplate.SelectedItem != null && lstCompanies.SelectedItem != null;

        private void Search()
        {
            var companies = DocumentAccess.Instance.SearchModuleRefs(txtSearchField.Text);
            lstCompanies.ItemsSource = companies;
        }
        

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this)?.Close();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Parent is Window)
                ((Window) Parent).Title = _windowTitle;

        }

        private void txtSearchField_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox text = sender as TextBox;
            if (text != null)
            {
                if (text.Text.Length >= 3)
                {
                    var companies = DocumentAccess.Instance.SearchModuleRefs(txtSearchField.Text);
                    lstCompanies.ItemsSource = companies;
                }
            }
        }
    }
}
