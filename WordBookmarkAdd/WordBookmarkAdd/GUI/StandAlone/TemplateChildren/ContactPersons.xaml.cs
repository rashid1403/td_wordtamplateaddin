﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WordBookmarkAdd.DAL;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.Util;

namespace WordBookmarkAdd.GUI.StandAlone.TemplateChildren
{
    /// <summary>
    /// Interaction logic for ContactPersons.xaml
    /// </summary>
    public partial class ContactPersons : UserControl
    {
        public TemplateParent TParent;
        private ObservableCollection<CustomerContact> Contacts = new ObservableCollection<CustomerContact>();

        public ObservableCollection<CustomerContact> Selected { get; private set; } =
            new ObservableCollection<CustomerContact>();

        private RelayCommand _nextCommand;

        private long CompanyID => (TParent.FirstWindow.lstCompanies.SelectedItem as DocModulRef).No.Value;

        public ContactPersons()
        {
            InitializeComponent();


            lsbContacts.DisplayMemberPath = nameof(CustomerContact.Name);
            lsbSelected.DisplayMemberPath = nameof(CustomerContact.Name);

            lsbContacts.ItemsSource = Contacts;
            lsbSelected.ItemsSource = Selected;

            btnNext.Command = _nextCommand = new RelayCommand(param => NextExecuted());
        }

        public void Init()
        {
            Contacts.Clear();
            Selected.Clear();
            CustomerAccess.Instance.SearchForCustomersContact(txtSearchField.Text, CompanyID)
                .ForEach(i => Contacts.Add(i));
        }

        private void NextExecuted()
        {
            TParent.SecondWindowNextBack(true);
        }



        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            TParent.SecondWindowNextBack(false);
        }

        private void lsbContacts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (CustomerContact item in e.AddedItems)
            {
                selectedAddItem(item);
                Contacts.Remove(item);
            }
        }

        private void selectedAddItem(CustomerContact item)
        {
            if (!Selected.Contains(item))
                Selected.Add(item);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Contacts.Clear();
            CustomerAccess.Instance.SearchForCustomersContact(txtSearchField.Text, CompanyID)
                .ForEach(i => Contacts.Add(i));
        }

        private void lsbSelected_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (CustomerContact item in e.AddedItems)
            {
                Selected.Remove(item);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this)?.Close();
        }

        private void txtSearchField_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox text = sender as TextBox;
            if (text != null)
            {
                if (text.Text.Length >= 3)
                {
                    Contacts.Clear();
                    CustomerAccess.Instance.SearchForCustomersContact(txtSearchField.Text, CompanyID)
                .ForEach(i => Contacts.Add(i));
                }
            }
        }
    }
}
