﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WordBookmarkAdd.DAL;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.Util;
using UserControl = System.Windows.Controls.UserControl;
using Window = System.Windows.Window;

namespace WordBookmarkAdd.GUI.StandAlone.TemplateChildren
{
    /// <summary>
    /// Interaction logic for VisitedBy.xaml
    /// </summary>
    public partial class VisitedBy : UserControl
    {
        private ObservableCollection<login> Logins = new ObservableCollection<login>();
        public ObservableCollection<login> Selected { get; private set; } = new ObservableCollection<login>();
        public TemplateParent TParent { get; set; }

        public VisitedBy()
        {
            InitializeComponent();
            lsbEmployee.ItemsSource = Logins;
            lsbSelected.ItemsSource = Selected;

            LoginAccess.Instance.GetLogins().ForEach(l => Logins.Add(l));
            login usr;
            if((usr = LoginAccess.Instance.GetCurrentUser())!= null)
            {
                Selected.Add(usr);
            }
            btnOK.Command = new RelayCommand(param => OkExecuted(), param => OKCanExecute);

            this.lsbSelected.DisplayMemberPath = nameof(login.UserName);
            this.lsbEmployee.DisplayMemberPath = nameof(login.UserName);
        }

        private void OkExecuted()
        {
            TParent.ThirdWindowOkBack(true);
        }

        private bool OKCanExecute => Selected.Any();


        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            TParent.ThirdWindowOkBack(false);
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            Logins.Clear();
            LoginAccess.Instance.SearchLogin(txtSearchField.Text).ForEach(l => Logins.Add(l));
        }

        private void lsbEmployee_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (login item in e.AddedItems)
            {
                selectedAddItem(item);
                Logins.Remove(item);
            }
        }

        private void selectedAddItem(login item)
        {
            if (!Selected.Contains(item))
                Selected.Add(item);
        }

        private void lsbSelected_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (login item in e.AddedItems)
            {
                Selected.Remove(item);
            }
        }

        private void bttnCancel_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this)?.Close();
        }

        private void txtSearchField_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox text = sender as TextBox;
            if (text != null)
            {
                if (text.Text.Length >= 3)
                {
                    Logins.Clear();
                    LoginAccess.Instance.SearchLogin(txtSearchField.Text).ForEach(l => Logins.Add(l));
                }
            }
        }
    }
}
