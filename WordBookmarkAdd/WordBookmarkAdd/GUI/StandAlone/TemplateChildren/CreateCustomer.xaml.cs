﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WordBookmarkAdd.DAL;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.DTO;
using WordBookmarkAdd.Util;
using Exception = System.Exception;
using UserControl = System.Windows.Controls.UserControl;

namespace WordBookmarkAdd.GUI.StandAlone.TemplateChildren
{
    /// <summary>
    /// Interaction logic for CreateCustomer.xaml
    /// </summary>
    public partial class CreateCustomer : UserControl
    {

        private List<ModuleContact> _contact;

        private ObservableCollection<DocModule> cmbModuleCollection;

        private ReadableGUIMessages _messageHandler;

        public CreateCustomer()
        {
            InitializeComponent();
            SetupMessageDisplayForChildren();
        }

        private void SetupMessageDisplayForChildren()
        {
            _messageHandler = new ReadableGUIMessages((s => txbErrorStatus.Text = s));
            ViewCreateCompany.MessageHandler = _messageHandler;
            ViewCreateContact.MessageHandler = _messageHandler;
        }

        public static void ShowWindow(Tuple<int, int> mouseXY)
        {
            var window = new Window
            {
                Left = mouseXY.Item1,
                Top = mouseXY.Item2,
                Content = new CreateCustomer(),
                SizeToContent = SizeToContent.WidthAndHeight,
                ResizeMode = ResizeMode.NoResize
            };
            window.ShowDialog();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Parent is Window)
                ((Window)Parent).Title = "CRM System";
        }

        public static void CreateWindow()
        {
            var window = new Window
            {
                Content = new CreateCustomer(),
                SizeToContent = SizeToContent.WidthAndHeight,
                ResizeMode = ResizeMode.NoResize
            };
            window.ShowDialog();
        }
    }
}
