﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using WordBookmarkAdd.BE;
using WordBookmarkAdd.BLL;
using WordBookmarkAdd.DAL;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.GUI.StandAlone.TemplateChildren;
using WordBookmarkAdd.Util;
using Window = System.Windows.Window;

namespace WordBookmarkAdd.GUI.StandAlone
{
    /// <summary>
    /// Interaction logic for TemplateParent.xaml
    /// </summary>
    public partial class TemplateParent : UserControl
    {
        public CompanySelection FirstWindow { get; private set; } = new CompanySelection();
        public ContactPersons SecondWindow { get; private set; } = new ContactPersons();
        public VisitedBy ThirdWindow { get; private set; } = new VisitedBy();


        public TemplateParent()
        {
            InitializeComponent();
            this.Content = FirstWindow;

            FirstWindow.TParent = this;
            SecondWindow.TParent = this;
            ThirdWindow.TParent = this;
        }

        public void FirstWindowNext()
        {
            Content = SecondWindow;
            SecondWindow.Init();
        }

        public void SecondWindowNextBack(bool next)
        {
            if (next)
            {
                Content = ThirdWindow;
            }
            else
            {
                Content = FirstWindow;
            }
        }

        public void ThirdWindowOkBack(bool ok)
        {
            if (ok)
            {
                OkExecuted();
            }
            else
            {
                Content = SecondWindow;
            }
        }

        public static void CreateWindow()
        {
            var window = new Window
            {
                Content = new TemplateParent(),
                SizeToContent = SizeToContent.WidthAndHeight,
                ResizeMode = ResizeMode.NoResize
            };
            window.Show();
        }

        public void OkExecuted()
        {
            if (File.Exists(FirstWindow.cmbTemplate.SelectedValue as string))
            {
                var selectedCompany = FirstWindow.lstCompanies.SelectedItem as DocModulRef;
                var selectedTemplate = FirstWindow.cmbTemplate.SelectedItem as DocTemplates;

                var selectedContacts = SecondWindow.Selected.ToList();
                var selectedVisitedBy = ThirdWindow.Selected.ToList();


                // Dunno, if it should open it here
                var template = Globals.ThisAddIn.Application.Documents.Add(FirstWindow.cmbTemplate.SelectedValue);
                var properties = template.CustomDocumentProperties as DocumentProperties;

                if (properties != null)
                {
                    foreach (DocumentProperty prop in properties)
                    {
                        // Check for value name instead of just deleting them all?
                        prop.Delete();
                    }
                }

                var customer = CustomerAccess.Instance.GetCustomer(selectedCompany?.No);
                CustomerContact contact = null;
                if (selectedContacts.Any())
                {
                    contact = selectedContacts[0];
                    
                }
                customer.ContactName = contact==null?"":contact.Name;
                customer.ContactMobile = contact == null ? "" : contact.Mobile;
                customer.ContactPhone = contact == null ? "" : contact.Phone;
                customer.ContactMail = contact == null ? "" : contact.Mail;

                Dictionary<string, string> customerProperties = customer.GetType()
                    .GetProperties()
                    .Where(p => p.CanRead && p.PropertyType == typeof(string))
                    .ToDictionary(p => p.Name, p => (string)p.GetValue(customer, null));

                // Adding various properties to the custom properties of the word document
                if (selectedTemplate?.Module != null) CustomDocProps.DocumentModule = selectedTemplate.Module.Value;
                CustomDocProps.CompanyNo = selectedCompany.No.Value;
                CustomDocProps.CompanyModule = selectedCompany.Modul.Value;
                CustomDocProps.CompanyRefId = selectedCompany.ID;

                // Loop through all the properties in the customerProperties and add them to the word document
                foreach (var keyValuePair in customerProperties)
                    typeof(CustomDocProps).GetProperty(keyValuePair.Key, BindingFlags.Public | BindingFlags.Static)?.SetValue(null, keyValuePair.Value);

                foreach (Bookmark bm in template.Bookmarks.Cast<Bookmark>().ToList())
                {
                    foreach (var keyValuePair in customerProperties)
                    {
                        if (bm.Name.TrimEnd('1', '2', '3', '4', '5', '6', '7', '8', '9', '0') ==
                            nameof(BE_Customer.ContactName))
                        {
                            string contacts = "";
                            if (selectedContacts.Any())
                            {
                                contacts = selectedContacts.Select(con => con.Name).Aggregate((acc, ite) => acc != "" ? acc + ", " + ite : ite);
                            }
                            WordLogic.Instance.UpdateBookmark(template,bm,contacts);
                        }
                        else if (bm.Name.TrimEnd('1', '2', '3', '4', '5', '6', '7', '8', '9', '0') == keyValuePair.Key)
                        {
                            if (keyValuePair.Value == null) break;
                            WordLogic.Instance.UpdateBookmark(template, bm, keyValuePair.Value);
                            break;
                        }
                    }

                    switch (bm.Name)
                    {
                        case "Date":
                            WordLogic.Instance.UpdateBookmark(template, bm, DateTime.Today.ToString("D", Thread.CurrentThread.CurrentCulture));
                            break;
                        case "DocumentCreator":
                            //selects all the UserName properties and aggregate them
                            //if the acc is "" assigns adUsr to acc otherwise assigns str + ", " adUsr to acc.
                            WordLogic.Instance.UpdateBookmark(template, bm, selectedVisitedBy
                                .Select(adUsr => adUsr.UserName)
                                .Aggregate((str, adUsr) => str !=""? str +", "+ adUsr:adUsr));
                            break;
                    }
                }
                Window.GetWindow(this)?.Close();
            }
            else
            {
                MessageBox.Show("File was not found. File location may be invalid", "Word Templates",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
    }
}
