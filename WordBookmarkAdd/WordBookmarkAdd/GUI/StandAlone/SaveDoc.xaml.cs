﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Office.Interop.Word;
using WordBookmarkAdd.BLL;
using WordBookmarkAdd.DAL;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.Logging;
using WordBookmarkAdd.Util;
using Window = System.Windows.Window;


namespace WordBookmarkAdd.GUI.StandAlone
{
    /// <summary>
    /// Interaction logic for SaveDoc.xaml
    /// </summary>
    public partial class SaveDoc : UserControl
    {
        private readonly Document _activeDocument = Globals.ThisAddIn.Application.ActiveDocument;
        private readonly IActivityAccess _activityAccess = ActivityAccess.Instance;
        private readonly ILoginAccess _loginAccess = LoginAccess.Instance;
        private readonly IQuotationAccess _quotationAccess = QuotationAccess.Instance;
        private readonly WordLogic _wordLogic = WordLogic.Instance;
        private readonly DocLocation _docLocation = new DocLocation();
        private readonly List<string> _dateShortString = new List<string>();
        private RelayCommand _saveAndCloseCommand;
        private int _previousStartDateIndex;
        private string _windowTitle = "Create Activity";
        private long? _activityID = null;

        public static readonly DependencyProperty IsOwnWindowProperty = DependencyProperty.Register("IsOwnWindow", typeof(bool),
                        typeof(SaveDoc), new FrameworkPropertyMetadata(true, IsOwnWindowChanged));

        public static readonly DependencyProperty CanSaveProperty = DependencyProperty.Register("CanSave", typeof(bool),
                        typeof(SaveDoc), new FrameworkPropertyMetadata(false));

        public static void CreateWindow()
        {
            var window = new Window
            {
                Content = new SaveDoc(),
                SizeToContent = SizeToContent.WidthAndHeight,
                ResizeMode = ResizeMode.NoResize
            };
            window.ShowDialog();
        }

        public bool CanSave
        {
            get { return (bool)GetValue(CanSaveProperty); }
            set { SetValue(CanSaveProperty, value); }
        }

        private static void IsOwnWindowChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            SaveDoc This = source as SaveDoc;

            if ((bool)e.NewValue)
            {
                This.btnSaveAndClose.Visibility = Visibility.Visible;
                This.btnSaveAndClose.IsEnabled = true;
            }
            else
            {
                This.btnSaveAndClose.Visibility = Visibility.Collapsed;
                This.btnSaveAndClose.IsEnabled = false;
            }
        }

        public bool IsOwnWindow
        {
            get { return (bool)GetValue(IsOwnWindowProperty); }
            set { SetValue(IsOwnWindowProperty, value); }
        }

        public SaveDoc()
        {
            InitializeComponent();

            txtMessage.Text = _wordLogic.GetBookmarkText(_activeDocument, "Text")?.Trim();
            txtSubject.Text = _wordLogic.GetBookmarkText(_activeDocument, "Headline")?.Trim();

            var dateTime = DateTime.Today;

            for (var time = dateTime; time < dateTime.AddHours(24); time = time.AddMinutes(30))
                _dateShortString.Add(time.ToShortTimeString());

            cmbFollowupDate.ItemsSource = _dateShortString;

            cmbFollowupDate.SelectedIndex = 0;

            dpFollowupDate.SelectedDate = dateTime;
            _previousStartDateIndex = 0;

            cmbType.DisplayMemberPath = nameof(CustomerActivityType.ActivityType);
            cmbType.SelectedValuePath = nameof(CustomerActivityType.ID);

            cmbResponsible.DisplayMemberPath = nameof(login.UserName);
            cmbResponsible.SelectedValuePath = nameof(login.ID);

            cmbDocumentType.DisplayMemberPath = nameof(DocStructure.Name);
            cmbDocumentType.SelectedValuePath = nameof(DocStructure.ID);

            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                // Database calls
                cmbType.ItemsSource = _activityAccess.FetchActivityTypes();
                cmbResponsible.ItemsSource = _loginAccess.GetLogins();
                cmbDocumentType.ItemsSource = DocumentAccess.Instance.GetStructures(1);
            }
            btnSaveAndClose.Command = SaveAndCloseCommand;
        }

        private ICommand SaveAndCloseCommand
        {
            get
            {
                return _saveAndCloseCommand ??
                       (_saveAndCloseCommand =
                           new RelayCommand(param => SaveAndCloseExecuted(), param => SaveAndCloseCanExecute));
            }
        }

        private void SaveAndCloseExecuted()
        {
            var cusActivity = new CustomerActivity();
            var doc = new DAL.EF.Documents();
            var date = DateTime.Now;
            try
            {
                //long documentModule = CustomDocProps.DocumentModule.Value;
                string companyName = CustomDocProps.CompanyName;

                DateTime followUpDate =
                    dpFollowupDate.SelectedDate.GetValueOrDefault().Add(TimeSpan.Parse(cmbFollowupDate.Text));

                if (string.IsNullOrEmpty(txtMessage.Text))
                {

                    var responsibleId = (long) cmbResponsible.SelectedValue;

                    cusActivity = new CustomerActivity
                    {
                        Type = (long) cmbType.SelectedValue,
                        Customer = CustomDocProps.CompanyNo,
                        Responsible = responsibleId,
                        Headline = txtSubject.Text.Trim(),
                        Text = txtMessage.Text.Trim(),
                        Date = DateTime.Now,
                        Followup = followUpDate
                    };

                    _docLocation.DocInformationReader("Customer", companyName,
                        ((DocStructure) cmbDocumentType.SelectedItem).Name);

                    doc.No = CustomDocProps.CompanyNo;
                    doc.Customer = CustomDocProps.CompanyModule == 1 ? new long?(CustomDocProps.CompanyNo.Value) : null;
                    doc.Module = CustomDocProps.CompanyModule;
                    doc.RefID = CustomDocProps.CompanyRefId;
                    doc.DocStructure = (long) cmbDocumentType.SelectedValue;
                    doc.CreatedBy = LoginAccess.Instance.GetCurrentUser()?.ID;
                    doc.FileName =
                        $"<a href=\"file:{_docLocation.DocumentLocation()}\\{_docLocation.FileName}\">{_docLocation.FileName}</a>";
                    doc.Reference = "";
                    doc.Description = cusActivity.Text;
                    doc.DocPath = null;

                    doc.Created = DateTime.Now;
                    doc.Date = followUpDate;
                }
                else
                {
                    _docLocation.DocInformationReader("Customer", companyName,
                       ((DocStructure)cmbDocumentType.SelectedItem).Name);

                    doc.No = CustomDocProps.CompanyNo;
                    doc.Customer = CustomDocProps.CompanyModule == 1 ? new long?(CustomDocProps.CompanyNo.Value) : null;
                    doc.Module = CustomDocProps.CompanyModule;
                    doc.RefID = CustomDocProps.CompanyRefId;
                    doc.DocStructure = (long)cmbDocumentType.SelectedValue;
                    doc.CreatedBy = LoginAccess.Instance.GetCurrentUser()?.ID;
                    doc.FileName =
                        $"<a href=\"file:{_docLocation.DocumentLocation()}\\{_docLocation.FileName}\">{_docLocation.FileName}</a>";
                    doc.Reference = "";
                    doc.Description = cusActivity.Text;
                    doc.DocPath = null;

                    doc.Created = DateTime.Now;
                    doc.Date = followUpDate;
                }
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
            }

            try
            {
                _activityID = _activityAccess.SaveActivity(cusActivity);
                if (((DocStructure)cmbDocumentType.SelectedItem).Name == "Quotation")
                    //CreateAndSaveQuotation(followUpDate);
                    CreateAndSaveQuotation(date);
                Bookmark bmResponsible = _wordLogic.GetBookmark(_activeDocument, "ActivityResponsible");
                if (bmResponsible != null)
                    _wordLogic.UpdateBookmark(_activeDocument, bmResponsible, ((login)cmbResponsible.SelectedItem).UserName);
                Bookmark bmActivityDate = _wordLogic.GetBookmark(_activeDocument, "ActivityDate");
                if (bmActivityDate != null)
                    _wordLogic.UpdateBookmark(_activeDocument, bmActivityDate, date.ToString("D", Thread.CurrentThread.CurrentCulture));

                doc.Activity = cusActivity.ID;
                doc.FollowUpDate = cusActivity.Followup;

                _activeDocument.SaveAs(_docLocation.DocumentLocation() + "\\" + _docLocation.FileName);
                DocumentAccess.Instance.SaveDocument(doc);

                if (IsOwnWindow)
                    Window.GetWindow(this)?.Close();
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                MessageBox.Show("An error occured while saving the activity", _windowTitle, MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void CreateAndSaveQuotation(DateTime followUpDate)
        {
            QuotationQuick qc = new QuotationQuick
            {
                Customer = CustomDocProps.CompanyNo,
                DateCreated = DateTime.Now,
                FollowUp = followUpDate,
                Name = CustomDocProps.ContactName,
                email = CustomDocProps.ContactMail
            };

            _quotationAccess.SaveQuotation(qc);
        }

        public bool SaveAndCloseCanExecute
        {
            get
            {
                CanSave = !string.IsNullOrWhiteSpace(txtSubject.Text)
                    && cmbType.SelectedItem != null && cmbResponsible.SelectedItem != null
                    && cmbDocumentType.SelectedItem != null;
                return CanSave;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Parent is Window)
                ((Window)Parent).Title = _windowTitle;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            _previousStartDateIndex = cmbFollowupDate.SelectedIndex;

            cmbFollowupDate.IsEnabled = false;

            cmbFollowupDate.SelectedIndex = 0;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            cmbFollowupDate.IsEnabled = true;

            cmbFollowupDate.SelectedIndex = _previousStartDateIndex;

            if (_previousStartDateIndex == 0)
                cmbFollowupDate.SelectedIndex = 0;
        }

        public DateTime? GetStartDate()
        {
            if (SaveAndCloseCanExecute)
                return dpFollowupDate.SelectedDate.GetValueOrDefault().Add(TimeSpan.Parse(cmbFollowupDate.Text));
            return null;
        }

        public long? SaveStuff()
        {
            SaveAndCloseExecuted();
            return _activityID;
        }
    }
}
