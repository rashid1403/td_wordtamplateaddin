﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WordBookmarkAdd.DAL;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.DTO;
using WordBookmarkAdd.Util;

namespace WordBookmarkAdd.GUI.StandAlone.SaveDocumentControls
{
    /// <summary>
    /// Interaction logic for CreateCompany.xaml
    /// </summary>
    public partial class CreateCompany : UserControl
    {
        public DocModulRef Company { get; private set; }

    public delegate void CompanyCreatedHandler(object sender, EventArgs args);
    public event CompanyCreatedHandler CompanyCreated;
    private bool CompanyCreatedFlag;

    public ReadableGUIMessages MessageHandler { get; set; } = ReadableGUIMessages.DefaultMessageHandler;

    private enum Mode
    {
        Customer,
        Vendor
    }
    private Mode DisplayMode = Mode.Customer;

        public CreateCompany()
        {
            InitializeComponent();
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                btnSaveCompany.Command = new RelayCommand(btnSaveCompanyClicked, btnSaveCompanyCanClick);
                cmbStatus.ItemsSource = CustomerAccess.Instance.FindAllStatuses();
                cmbGroup.ItemsSource = CustomerAccess.Instance.FindAllCustomerGroups();
                cmbResponsible.ItemsSource = LoginAccess.Instance.GetLogins();
                using (var context = DBContextHandler.GetNewDBInstance())
                {
                    cmbCountry.ItemsSource = context.Country.ToList();
                }
                
                setComboBoxPaths();

                cmbType.ItemsSource = new List<Tuple<string, long>>
                {
                    Tuple.Create("Customer",1L),
                    Tuple.Create("Vendor",2L)
                };

                cmbGroup.SelectionChanged += SetSubGroups;
                cmbType.SelectionChanged += AlterDisplay;
            }
        }

        private void setComboBoxPaths()
        {
            //name
            cmbGroup.DisplayMemberPath = nameof(Tuple<string, long>.Item1);
            //id
            cmbGroup.SelectedValuePath = nameof(Tuple<string, long>.Item2);

            cmbSubgroup.DisplayMemberPath = nameof(Tuple<string, long>.Item1);
            cmbSubgroup.SelectedValuePath = nameof(Tuple<string, long>.Item2);

            cmbStatus.DisplayMemberPath = nameof(Tuple<string, long>.Item1);
            cmbStatus.SelectedValuePath = nameof(Tuple<string, long>.Item2);

            cmbType.DisplayMemberPath = nameof(Tuple<string, long>.Item1);
            cmbType.SelectedValuePath = nameof(Tuple<string, long>.Item2);

            cmbCountry.DisplayMemberPath = nameof(Country.Country1);
            cmbCountry.SelectedValuePath = nameof(Country.ID);

            cmbZipCode.DisplayMemberPath = nameof(Tuple<string, long>.Item1);
            cmbZipCode.SelectedValuePath = nameof(Tuple<string, long>.Item2);

            cmbResponsible.DisplayMemberPath = nameof(User.Name);
            cmbResponsible.SelectedValuePath = nameof(User.Id);
        }

        private void AlterDisplay(object sender, SelectionChangedEventArgs args)
        {
            if (cmbType.SelectedItem != null)
            {
                switch ((long)cmbType.SelectedValue)
                {
                    case 1:
                        cmbSubgroup.Visibility = Visibility.Visible;
                        cmbCountry.Visibility = Visibility.Visible;
                        txtVendorNo.Visibility = Visibility.Collapsed;
                        DisplayMode = Mode.Customer;
                        break;
                    case 2:
                        cmbSubgroup.Visibility = Visibility.Collapsed;
                        cmbCountry.Visibility = Visibility.Collapsed;
                        txtVendorNo.Visibility = Visibility.Visible;
                        DisplayMode = Mode.Vendor;
                        break;
                }
            }
        }

        private void SetSubGroups(object sender, SelectionChangedEventArgs e)
        {
            if (cmbGroup.SelectedItem != null)
            {
                cmbSubgroup.ItemsSource = CustomerAccess.Instance.FindAllCustomerSubGroups((long)cmbGroup.SelectedValue);
            }
            e.Handled = false;
        }

        private bool btnSaveCompanyCanClick(object obj)
        {
            return cmbType.SelectedItem != null &&
               cmbGroup.SelectedItem != null &&
               !string.IsNullOrWhiteSpace(txtName.Text) &&
               (DisplayMode == Mode.Customer || !string.IsNullOrWhiteSpace(txtVendorNo.Text)) &&
               !CompanyCreatedFlag;
        }

        private void btnSaveCompanyClicked(object obj)
        {
            Customer cust = null;
            Vendor vend = null;
            switch ((long)cmbType.SelectedValue)
            {
                case 1: //TODO hardcoded value based on database ID :( (1 is customer)
                    cust = SaveCustomer();
                    break;
                case 2:
                    vend = SaveVendor();
                    break;
            }

            DocModulRef moduleRef = new DocModulRef();
            if (cust != null)
            {
                moduleRef.Modul = 1;
                moduleRef.Name = cust.Name;
                moduleRef.No = cust.ID;
            }
            else if (vend != null)
            {
                moduleRef.Modul = 2;
                moduleRef.Name = vend.Name;
                moduleRef.No = vend.ID;
            }
            else
            {
                return;
            }
            //try
            //{
            //    DocumentAccess.Instance.SaveModuleRef(moduleRef);
            //    try
            //    {
            //        if (!string.IsNullOrWhiteSpace(txtDomain.Text))
            //            CustomerAccess.Instance.AddCustomerDomains(txtDomain.Text, moduleRef.ID);
            //    }
            //    catch (Exception exe)
            //    {
            //        #region stupidLongExceptionHandling
            //        bool handled = false;
            //        for (Exception inner = exe; inner != null; inner = inner.InnerException)
            //        {
            //            var sqlExe = inner as SqlException;
            //            if (sqlExe == null) continue;
            //            if (sqlExe.Number == 2627)
            //            {
            //                MessageHandler.ShowMessage($"Company Saved. Domain {txtDomain.Text} already referenced an existing company,\nno domain refence not added.");
            //                handled = true;
            //            }
            //            else
            //                throw;
            //        }
                    //if (!handled)
                    //    throw;
                    //#endregion
                //}
            //    Company = moduleRef;
            //    CompanyCreated?.Invoke(this, EventArgs.Empty);
            //}
            //catch (Exception exe)
            //{
            //    if (!MessageHandler.TryHandle(exe))
            //        throw;
            //}
        }


        private Vendor SaveVendor()
        {
            Vendor vend = new Vendor
            {
                Name = txtName.Text,
                VendorGroup = (long)cmbGroup.SelectedValue,
                Status = (long?)cmbStatus.SelectedValue,
                Responsible = (long?)cmbResponsible.SelectedValue,
                Adress1 = string.IsNullOrWhiteSpace(txtRoadAndNR.Text) ? null : txtRoadAndNR.Text,
                City = string.IsNullOrWhiteSpace(txtCity.Text) ? null : txtCity.Text,
                VendorNo = txtVendorNo.Text
            };  
            vend.Zip = cmbZipCode.SelectedValue as long?;
               
            try
            {
                VendorAccess.Instance.AddVendor(vend);
                ChangeToPostSaveState();
                return vend;
            }
            catch (Exception exe)
            {
                if (!MessageHandler.TryHandle(exe))
                    throw;
            }
            return null;
        }

        private Customer SaveCustomer()
        {
            long? zip = cmbZipCode.SelectedValue as long?;

            Customer cust = new Customer();

            cust.Name = txtName.Text;
            cust.CustomerGroup = (long) cmbGroup.SelectedValue;
            cust.Status = (long?) cmbStatus.SelectedValue;
            cust.CustomerSubGroup = (long?) cmbSubgroup.SelectedValue;
            cust.Adress1 = string.IsNullOrWhiteSpace(txtRoadAndNR.Text) ? null : txtRoadAndNR.Text;
            cust.City = string.IsNullOrWhiteSpace(txtCity.Text) ? null : txtCity.Text;
            cust.Zip = zip;
            cust.Country = (long?) cmbCountry.SelectedValue;
            

            try
            {
                CustomerAccess.Instance.AddNewCustomer(cust);
                ChangeToPostSaveState();
                return cust;
            }
            catch (Exception exe)
            {
                if (!MessageHandler.TryHandle(exe))
                    throw;
            }

            return null;
        }

        private void ChangeToPostSaveState()
        {
            CompanyCreatedFlag = true;
            btnSaveCompany.Visibility = Visibility.Collapsed;
            MessageHandler.ShowMessage($"{(DisplayMode == Mode.Customer ? "Customer" : "vendor")} saved to database");
        }

        private void cmbCountry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                 var list =
                    context.CountryZipCode
                    .Where(z => z.Country == (long)cmbCountry.SelectedValue)
                    .OrderBy(zip => zip.City)
                    .Select(zip => new {zip.City,zip.Zip,zip.ID})
                    .ToList();

                cmbZipCode.ItemsSource = list.Select(zip => Tuple.Create($"{zip.City} ({zip.Zip})", zip.ID)).ToList();

            }
        }
    }
}