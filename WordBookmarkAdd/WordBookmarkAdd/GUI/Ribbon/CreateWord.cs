﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using WordBookmarkAdd.DAL.EF;
using WordBookmarkAdd.GUI.StandAlone;
using WordBookmarkAdd.BLL;
using WordBookmarkAdd.GUI.StandAlone.TemplateChildren;

namespace WordBookmarkAdd
{
    public partial class CreateWord
    {
        public static DocLocation doc;

        private void CreateWord_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void btnDatabaseSettings_Click(object sender, RibbonControlEventArgs e)
        {
            DatabaseConnection.CreateWindow();
        }

        private void btnOpen_Click(object sender, RibbonControlEventArgs e)
        {
            TemplateParent.CreateWindow();
        }

        private void btnSave_Click(object sender, RibbonControlEventArgs e)
        {
            SaveDoc.CreateWindow();
        }

        private void btnCreateCustomer_Click(object sender, RibbonControlEventArgs e)
        {
            CreateCustomer.CreateWindow();
        }

        //private void btnAddCustomer_Click(object sender, RibbonControlEventArgs e)
        //{
        //    CreateCustomer.CreateWindow();
        //}
    }
}
