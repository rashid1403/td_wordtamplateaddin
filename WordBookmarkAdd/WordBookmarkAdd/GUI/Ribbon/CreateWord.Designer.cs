﻿namespace WordBookmarkAdd
{
    partial class CreateWord : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public CreateWord()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.grpSettings = this.Factory.CreateRibbonGroup();
            this.btnDatabaseSettings = this.Factory.CreateRibbonButton();
            this.grpTemplate = this.Factory.CreateRibbonGroup();
            this.btnOpen = this.Factory.CreateRibbonButton();
            this.btnSave = this.Factory.CreateRibbonButton();
            this.btnAddCustomer = this.Factory.CreateRibbonButton();
            this.btnCreateCustomer = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.grpSettings.SuspendLayout();
            this.grpTemplate.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.Groups.Add(this.grpSettings);
            this.tab1.Groups.Add(this.grpTemplate);
            this.tab1.Label = "TD Templates";
            this.tab1.Name = "tab1";
            // 
            // grpSettings
            // 
            this.grpSettings.Items.Add(this.btnDatabaseSettings);
            this.grpSettings.Label = "Settings";
            this.grpSettings.Name = "grpSettings";
            // 
            // btnDatabaseSettings
            // 
            this.btnDatabaseSettings.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnDatabaseSettings.Label = "Edit Database Settings";
            this.btnDatabaseSettings.Name = "btnDatabaseSettings";
            this.btnDatabaseSettings.OfficeImageId = "FileCompactAndRepairDatabase";
            this.btnDatabaseSettings.ScreenTip = "Modify Database Connection";
            this.btnDatabaseSettings.ShowImage = true;
            this.btnDatabaseSettings.SuperTip = "Displays a dialog that allows for modification of the database connection.";
            this.btnDatabaseSettings.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnDatabaseSettings_Click);
            // 
            // grpTemplate
            // 
            this.grpTemplate.Items.Add(this.btnOpen);
            this.grpTemplate.Items.Add(this.btnSave);
            this.grpTemplate.Items.Add(this.btnAddCustomer);
            this.grpTemplate.Items.Add(this.btnCreateCustomer);
            this.grpTemplate.Label = "File Template";
            this.grpTemplate.Name = "grpTemplate";
            // 
            // btnOpen
            // 
            this.btnOpen.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnOpen.Label = "Open";
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.OfficeImageId = "ImportSavedImports";
            this.btnOpen.ShowImage = true;
            this.btnOpen.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnOpen_Click);
            // 
            // btnSave
            // 
            this.btnSave.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnSave.Label = "Save";
            this.btnSave.Name = "btnSave";
            this.btnSave.OfficeImageId = "ExportSavedExports";
            this.btnSave.ShowImage = true;
            this.btnSave.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSave_Click);
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Label = "";
            this.btnAddCustomer.Name = "btnAddCustomer";
            // 
            // btnCreateCustomer
            // 
            this.btnCreateCustomer.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnCreateCustomer.Label = "Create customer";
            this.btnCreateCustomer.Name = "btnCreateCustomer";
            this.btnCreateCustomer.OfficeImageId = "DistributionListAddNewMember";
            this.btnCreateCustomer.ShowImage = true;
            this.btnCreateCustomer.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnCreateCustomer_Click);
            // 
            // CreateWord
            // 
            this.Name = "CreateWord";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.CreateWord_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.grpSettings.ResumeLayout(false);
            this.grpSettings.PerformLayout();
            this.grpTemplate.ResumeLayout(false);
            this.grpTemplate.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpSettings;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnDatabaseSettings;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpTemplate;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnOpen;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSave;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAddCustomer;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnCreateCustomer;
    }

    partial class ThisRibbonCollection
    {
        internal CreateWord CreateWord
        {
            get { return this.GetRibbon<CreateWord>(); }
        }
    }
}
